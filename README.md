# Codeberg Pages

[![License: EUPL-1.2](https://img.shields.io/badge/License-EUPL--1.2-blue)](https://opensource.org/license/eupl-1-2/)
[![status-badge](https://ci.codeberg.org/api/badges/Codeberg/pages-server/status.svg)](https://ci.codeberg.org/Codeberg/pages-server)
<a href="https://matrix.to/#/#gitea-pages-server:matrix.org" title="Join the Matrix room at https://matrix.to/#/#gitea-pages-server:matrix.org">
  <img src="https://img.shields.io/matrix/gitea-pages-server:matrix.org?label=matrix">
</a>

Gitea не может размещать статические страницы из Git.
Сервер Codeberg Pages устраняет этот недостаток, внедряя автономный сервис, который подключается к Gitea через API.
Его также можно развернуть на других экземплярах Gitea, чтобы предложить своим пользователям хостинг статических страниц.

**Документацию для конечного пользователя** в основном можно найти на [Wiki](https://codeberg.org/Codeberg/pages-server/wiki/Overview)
и [Документация Codeberg](https://docs.codeberg.org/codeberg-pages/).


<a href="https://codeberg.org/Codeberg/pages-server"> <img src="https://codeberg.org/Codeberg/GetItOnCodeberg/raw/branch/main/get-it-on-blue-on-white.svg" alt="Get It On Codeberg" width="250"/> <a/>

## Быстрый старт

Это новый сервер Codeberg Pages, решение для обслуживания статических страниц из репозиториев Gitea.
Сопоставление пользовательских доменов больше не статично, его можно выполнить с помощью DNS:

1) добавьте в репозиторий текстовый файл `.domains`, содержащий разрешенные домены, разделенные новыми строками.
   первая строка будет каноническим доменом/URL; все остальные вхождения будут перенаправлены на него.

2) добавьте запись CNAME в свой домен, указывающую на `[[{branch}.]{repo}.]{owner}.codeberg.page` (по умолчанию репозиторий
«pages», «branch» по умолчанию соответствует ветке по умолчанию, если «repo» — это «pages», или «pages», если «repo» — это что-то другое.
Если имя ветки содержит символы косой черты, нужно заменить «/» в названии ветки на «~»):
  `www.example.org. В CNAME main.pages.example.codeberg.page.`

3) if a CNAME is set for "www.example.org", you can redirect there from the naked domain by adding an ALIAS record
for "example.org" (if your provider allows ALIAS or similar records, otherwise use A/AAAA), together with a TXT
record that points to your repo (just like the CNAME record):
  `example.org IN ALIAS codeberg.page.`
  `example.org IN TXT main.pages.example.codeberg.page.`
  
3) если для «www.example.org» установлено CNAME, вы можете перенаправить туда с «голого» домена, добавив запись ALIAS.
для «example.org» (если ваш провайдер разрешает ALIAS или аналогичные записи, в противном случае используйте A/AAAA) вместе с TXT
запись, указывающая на ваше репо (так же, как запись CNAME):
  `example.org В алиасе codeberg.page.`
  `example.org В TXT main.pages.example.codeberg.page.`

Сертификаты генерируются, обновляются и очищаются автоматически с помощью Let's Encrypt через вызов TLS.

## Чат для администраторов и разработчиков

[matrix: #gitea-pages-server:matrix.org](https://matrix.to/#/#gitea-pages-server:matrix.org)

## Развертывание

**Внимание: применяются некоторые предостережения**
> В настоящее время развертывание требует от вас определенных знаний в области системного администрирования, а также понимания и создания кода,
> так что вы можете в конечном итоге редактировать ненастраиваемые и специфичные для codeberg настройки.
> В будущем мы постараемся уменьшить их количество и сделать размещение Codeberg Pages таким же простым, как и настройку Gitea.
> Если вы планируете использовать Страницы на практике, пожалуйста, сначала свяжитесь с нами,
> затем мы попытаемся поделиться некоторыми основными шагами и задокументировать текущее использование для администраторов
> (может измениться в текущем состоянии).

Развертывание самого программного обеспечения очень просто. Вы можете взять бинарный файл текущей версии или собрать его самостоятельно,
настройте среду, как описано ниже, и все готово.

Сложность заключается в добавлении **поддержки собственного домена**, если вы собираетесь ее использовать.
SSL-сертификаты (запрос + продление) автоматически обрабатываются сервером страниц, но если вы хотите запустить его на общем IP-адресе (а не на отдельном),
вам нужно будет настроить обратный прокси-сервер, чтобы не разрывать соединения TLS, но пересылайте запросы на уровне IP на сервер страниц.

Вы можете проверить доказательство концепции в папке `haproxy-sni`,
и особенно взгляните на [этот раздел haproxy.cfg](https://codeberg.org/Codeberg/pages-server/src/branch/main/haproxy-sni/haproxy.cfg#L38).

### Окружение

- `HOST` & `PORT`   (по умолчанию: `[::]` & `443`): слушать адрес.
- `PAGES_DOMAIN`    (по умолчанию: `codeberg.page`): основной домен для страниц.
- `RAW_DOMAIN`      (по умолчанию: `raw.codeberg.page`): домен для raw ресурсов.
- `GITEA_ROOT`      (по умолчанию: `https://codeberg.org`): root (корень) вышестоящего экземпляра Gitea.
- `GITEA_API_TOKEN` (по умолчанию: empty): Токен API для экземпляра Gitea для доступа к закрытым (например, ограниченным) репозиториям..
- `RAW_INFO_PAGE`   (по умолчанию: https://docs.codeberg.org/pages/raw-content/): информационная страница для необработанных ресурсов, отображаемая, если ресурс не указан.
- `ACME_API`        (по умолчанию: https://acme-v02.api.letsencrypt.org/directory): установите для этого https://acme.mock.director, чтобы использовать недействительные сертификаты без какой-либо проверки (отлично подходит для отладки).
  ZeroSSL может быть лучше в будущем, поскольку он не имеет ограничений по скорости и не конфликтует с официальными сертификатами Codeberg (которые используют Let's Encrypt), но мне пока не удалось заставить его работать.
- `ACME_EMAIL`      (по умолчанию: `noreply@example.email`): Установите значение «true», чтобы принять Условия обслуживания вашего поставщика услуг ACME.
- `ACME_EAB_KID` &  `ACME_EAB_HMAC` (по умолчанию: don't use EAB): Учетные данные EAB, например, для ZeroSSL.
- `ACME_ACCEPT_TERMS` (по умолчанию: использовать самоподписанный сертификат): Установите значение «true», чтобы принять Условия обслуживания вашего поставщика услуг ACME.
- `ACME_USE_RATE_LIMITS` (по умолчанию: true): Установите для этого параметра значение false, чтобы отключить ограничения скорости, например. с ZeroSSL.
- `ENABLE_HTTP_SERVER` (по умолчанию: false): Установите значение true, чтобы включить вызов HTTP-01 и перенаправить все остальные HTTP-запросы на HTTPS. В настоящее время работает только с портом 80.
- `DNS_PROVIDER` (по умолчанию: использовать самоподписанный сертификат): Код провайдера ACME DNS для подстановочного знака основного домена.  
   См. https://go-acme.github.io/lego/dns/ для доступных значений и дополнительных переменных среды.
- `LOG_LEVEL` (по умолчанию: warn (предупреждать): Установите это, чтобы указать уровень ведения журнала.


## Вклад в развитие

Команда Codeberg очень открыта для вашего вклада.
Поскольку мы хорошо работаем в команде, иногда может быть трудно начать
(все еще проверяйте выпуски, мы всегда стремимся иметь что-то, что поможет вам начать работу).

Если у вас есть какие-либо вопросы, вы хотите поработать над функцией или представить себе сотрудничество с нами в течение некоторого времени,
не стесняйтесь пинговать нас в вопросе или в общей чат-группе Matrix.

Вы также можете связаться с сопровождающими этого проекта:

- [momar](https://codeberg.org/momar) [(Matrix)](https://matrix.to/#/@moritz:wuks.space)
- [6543](https://codeberg.org/6543) [(Matrix)](https://matrix.to/#/@marddl:obermui.de)

### Первые шаги

Код этого репозитория разбит на несколько модулей.
[Объяснение архитектуры](https://codeberg.org/Codeberg/pages-server/wiki/Architecture) in the wiki.

Папка `cmd` содержит данные, необходимые для взаимодействия со службой через cli.
Сердце программного обеспечения находится в папке «сервер» и разделено на несколько модулей.

Еще раз: не стесняйтесь обращаться к нам по любым вопросам, которые могут возникнуть.
Большое спасибо.

### Тестовый сервер
 
Убедитесь, что у вас установлен [golang](https://go.dev) v1.20 или новее и [just](https://just.systems/man/en/).

запустить `just dev`
теперь эти страницы должны работать:
 - https://cb_pages_tests.localhost.mock.directory:4430/images/827679288a.jpg
 - https://momar.localhost.mock.directory:4430/ci-testing/
 - https://momar.localhost.mock.directory:4430/pag/@master/
 - https://mock-pages.codeberg-test.org:4430/README.md
